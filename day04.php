<?php
echo "<!DOCTYPE html>
<html lang='en'>";

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    // Khai bao mang $error
    $error = array();
    if (empty(trim($_POST['username']))) {
        $error['username'] = "Hãy nhập tên";
    } else {
        $username = $_POST['username'];
    }
    if(empty(($_POST['gender']))) {
        $error['gender'] = "Hãy chọn giới tính";
    }else{
        $gender = $_POST['gender'];
    }

    if(empty($_POST['dpm'])) {
        $error['dpm'] = "Hãy chọn phân khoa";
    }else{
        $dpm = $_POST['dpm'];
    }
    if(!empty($_POST['date'])){
        if(!check($_POST['date'])){
            $error['date'] = "Nhập không đúng định dạng";
        }
    } 
}
    // check fomat date dd/mm/yyyy
function check($data) {
    $reg = "/^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[0-2])\/(19|20)\d{2}$/";
    if(preg_match($reg, $data)) {
        return true;
    }else{
        return false;
    }
}
    



echo "
<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Document</title>
    <!-- CSS only -->
    <style>
        .frames {
            width: 600px;
            height: 500px;
            border: solid #006ccb 2px;
            margin-left: 30%;
            margin-top: 30px;
        }

        .frames-margin {
            margin-top: 50px;
            margin-left: 25px;
        }

        .custom-table {
            border-spacing: 25px;
            border-spacing: 15px;
            
        }

        .display {
            width: 82px;
            height: 35px;
            padding-left: 15px;
            border: solid #006ccb 2px;
            color: #fff;
        }

        .display2 {
            width: 230px;
            height: 35px;
            padding-left: 15px;
            border: solid #006ccb 2px;
            color: #000;
        }
        .display3{
            width: 155px;
            height: 38px;
            padding-left: 15px;
            border: solid #006ccb 2px;
            
        }
        .checkbox {
            font-size: 25px;

        }

        .checkbox1 {
            width: 30px;
            height: 22px;
            background-color: aqua;
        }

        .checkbox2 {
            width: 30px;
            height: 22px;
            
        }

        .selectbox {
            width: 155px;
            height: 38px;
            padding-left: 15px;
            border: solid #006ccb 2px;

        }

        #signup {
            margin-left: 30%;

        }

        .customsignup {
            color: white;
            background-color: rgb(0, 162, 40);
            height: 50px;
            width: 140px;
            border: #006ccb;
            border-radius: 10px;
            font-size: 20px;
        }
        
        .label{
        color:red;
        }
        input[type='date']::-webkit-inner-spin-button,
        input[type='date']::-webkit-calendar-picker-indicator {
         display: none;
        -webkit-appearance: none;
                }
        }
    </style>
    
</head>



<body>
    <div class='frames'>
        <div class='frames-margin'>
            <div style=' margin-left: 20px; width: 450px;'>
            </div>
            <form action='' method='POST'>
            <table class='custom-table'>
                <tr>
                    <td id='usename' class='display' style='background-color: #1bb857;'>Họ và tên <label class='label'>*</label></td>
                    <td><input class='display2' type='text' name='username''></td>";
                            if (isset($error['username'])) {
                                echo"<span style = 'color: red;'>";
                                echo $error['username'] ;
                    
                                "</span>"; 
                            }
                            echo"
                            <br/>
                </tr>

                <tr>
                    <td class='display' style='background-color: #1bb857;'>Giới tính<label class='label'>*</label></td>
                    <td>";
                        $gioitinh = array(0 => 'Nam', 1 => 'Nữ');
                        for ($i = 0; $i <= count($gioitinh) - 1; $i++) {
                            echo
                            "<input class='checkbox1' type='radio' id='" . $i . "' class='gender' name='gender' value='" . $gioitinh[$i] . "'/> " . $gioitinh[$i];
                        }
                        echo" 
                        </td>"; 
                        if (isset($error['gender'])){
                            echo "<span style = 'color: red;'>"; echo $error['gender']; echo "</span>
                            "; }
                            echo "
                        <br/>


                </tr>

                <tr>
                    <td class='display' style='background-color: #1bb857;'>Phân khoa<label class='label'>*</label></td>
                    <td><select class='selectbox' name='dpm'>";
                            $khoa = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                            foreach ($khoa as $key => $value) {
                                echo "
                                 <option value='" . $key . "'>" . $value . "</option>";
                            }
                            echo "
                        </select></td>
                        "; if(isset($error['dpm'])){
                            echo "<span style = 'color:red;'>"; echo $error['dpm']; echo "</span>
                            "; }
                            echo"

                        <br/>

                </tr>
                <tr>
                    <td class='display' style='background-color: #1bb857;'>Ngày sinh<label class='label'>*</label></td>
                    <td><input class='display3' name = 'date' placeholder = 'dd/mm/yyyy'  type='text' value = ''></td>
                    ";
                    if (isset($error['date'])){
                    echo
                        "
                    <span style = 'color: red;'>"; echo $error['date'] ;
                    echo
                    "</span>"; }
                    echo
                        "
                    <br/>
                </tr>

                <tr>
                    <td class='display' style='background-color: #1bb857;'>Địa chỉ</td>
                    <td><input class='display2' type='text'></td>
                </tr>

            </table>

            <div id='signup'>
                <input class='customsignup' type='submit' value='Đăng ký'>
            </div>
        </div>
</body>

</html>";
